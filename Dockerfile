FROM alpine:latest

RUN apk -U add gpsd

RUN addgroup nobody dialout

EXPOSE 2947

CMD ["/usr/sbin/gpsd","-N", "-n", "-G", "-D", "9", "/dev/ttyS0"]

